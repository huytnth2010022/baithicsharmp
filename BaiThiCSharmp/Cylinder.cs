﻿using System;
using System.Collections.Generic;

namespace BaiThiCSharmp
{
    public class Cylinder
    {
        public List<double> Process()
        {
            List<double> listdoubel = new List<double>();
            Console.WriteLine("Enter the dimenstions of the cylinder");
            Console.WriteLine("Radius: ");
            double radius = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Height: ");
            double height = Convert.ToDouble(Console.ReadLine());
            double BaseArea = radius * radius * Math.PI;
            double LateralArea = 2 * Math.PI * radius * height;
            double TotalArea = 2 * Math.PI * radius * (height + radius);
            double Volume = Math.PI * radius * radius * height;
        listdoubel.Add(BaseArea);
        listdoubel.Add(LateralArea);
        listdoubel.Add(TotalArea);
        listdoubel.Add(Volume);
        listdoubel.Add(radius);
        listdoubel.Add(height);
        return listdoubel;
        }

        public void Result()
        {
            var list = Process();
            Console.WriteLine("Cylinder Characteristics");
            Console.WriteLine("Radius: {0:F}",list[4]);
            Console.WriteLine("Height: {0:F}",list[5]);
            Console.WriteLine("Base: {0:F}",list[0]);
            Console.WriteLine("LateralArea: {0:F}",list[1]);
            Console.WriteLine("TotalArea: {0:F}",list[2]);
            Console.WriteLine("Volume: {0:F}",list[3]);
            
            
        }
    }
}
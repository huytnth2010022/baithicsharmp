﻿using System;

namespace BaiThiCSharmp_2
{
    public abstract class Animal
    {
       public string name { get; set; }
       public double weight { get; set; }

       public abstract void Show();

       public abstract void SetMe(string name,double weight);
    }
}
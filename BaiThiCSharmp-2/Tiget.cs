﻿using System;

namespace BaiThiCSharmp_2
{
    public class Tiger:Animal
    {
        private string name;
        private double weight;
        public override void Show()
        {
            Console.WriteLine("Name: " + name);
            Console.WriteLine("Weight: " + weight);
        }

        public override void SetMe(string name, double weight)
        {
            this.name = name;
           this.weight= weight ;
        }
    }
}